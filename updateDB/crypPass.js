const bcrypt = require('bcrypt');
let fs = require("fs");

async function cryptPass () {
    let contents = fs.readFileSync("../updateDB/users1.json", 'utf8');

    const users = JSON.parse(contents);
    console.log(users);

    for (let user of users){
        if (user.password.length < 50){
           user.password = await bcrypt.hash(user.password, 12);
           console.log(user.password);
        }
    }

    const newHashedusers = JSON.stringify(users);

    fs.writeFile('../updateDB/users2.json', newHashedusers, function (err) {
        if (err) return console.log(err);
        console.log('Hello World > helloworld.txt');
    });
}

cryptPass();

// module.exports = crypto;