const {Router} = require('express');
const Questions = require('../models/Questions');
const Answer = require('../models/Answer');
const Stat = require('../models/Stat');
const router = Router();
const auth = require('../middleware/auth.middleware');
const valid = require('../middleware/valid.middleware');

router.post('/setquest', auth, valid, async (req, res) => {
    try {

        const person = req.user.person;
        const group = req.group;
        const activity = req.activity;
        const name = req.name;
        const questions = req.questions; //TODO можно и без констант ?
        const answers = Object.assign({}, questions);


        for (let answer in answers){
            answers[answer] = 0;
        }

        if (person !== 'admin'){
            return res.status(403).json({message: 'Ошибка прав доступа'});
        }

        const question = new Questions({group, questions, activity, name});
        const id = await question.save();


        const stat = new Stat({group, answers, questions, activity, name,count: 0, owner: req.user.userId, queLink: id._id} );
        const result = await stat.save();

        res.status(201).json({message: 'Список вопросов создан',result});


    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так попробуйте снова'})
    }
});

router.get('/getanswers',auth, async (req, res) => {

    try {
        const person = req.user.person;

        if (person !== 'student'){
            return res.status(403).json({message: 'Ошибка прав доступа'});
        }

        const group = req.user.group;
        const user =  await Questions.findOne({ group, activity: true});

        const candidate =  await Answer.findOne({ queLink: user._id });
        console.log(user._id);
        console.log(candidate);

        if (candidate){
            console.log(candidate);
            return res.status(200).json({coincidence: true});
        }

        return res.json({questions: user.questions, name: user.name, id: user._id});

    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так попробуйте снова',e: e})
    }


});



router.post('/setanswer', auth, valid, async (req, res) => {
    try {

        const person = req.user.person;
        const group = req.group;
        const activity = req.activity;
        const questions = req.questions; //TODO можно и без констант ?
        const name = req.name;
        const id = req.id;

        if (person !== 'student'){
            return res.status(403).json({message: 'Ошибка прав доступа'});
        }

        const answer = new Answer({group, answer: questions, activity,owner: req.user.userId,question : req.body.questionsData, name, queLink: id});
        await answer.save();

        let stat = await Stat.findOne({ queLink: id });
        stat = JSON.parse(JSON.stringify(stat));
        console.log(stat)
        let answers = stat.answers;
        let count = stat.count;
        count++;

        for (let ans in answers){
            answers[ans] = +(answers[ans]) + +(questions[ans]);
        }

        console.log(answers);
        const updatedStat = await Stat.updateOne({queLink: id}, {answers});
        await Stat.updateOne({queLink: id}, {count});

        console.log(updatedStat);

        res.status(201).json({answer,updatedStat});


    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так попробуйте снова', e: e})
    }
});

router.get('/:id', auth, async (req, res) => {
    const person = req.user.person;
    try {
        if (person === 'student'){
            const feedback = await Answer.findById(req.params.id);
            return res.json(feedback);
        }
        const stat = await Stat.findById(req.params.id);
        res.json(stat);
    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
    }
});

router.get('/', auth, async (req, res) => {
    const person = req.user.person;
    try {
        if (person === 'student') {
            const answer = await Answer.find({ owner: req.user.userId });
            return res.json(answer);
        }
        const answer = await Stat.find({ owner: req.user.userId });
        res.json(answer);
    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' });
        console.log(e)
    }
});

module.exports = router;