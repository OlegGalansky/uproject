const {Router} = require('express');
const {check,validationResult} = require('express-validator');
const bcrypt = require('bcrypt');
// const config = require('config');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const router = Router();


router.post('/register',
    [
      check('email','Некорректный email').isEmail(),
      check('password','Минимальная длина пароля 6 сиволов')
          .isLength({min: 6})
    ],
    async (req,res) => {
    try {
        console.log('Body',req.body);
        const errors = validationResult(req);

        if (!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message: 'Некорректные данные при регистрации'
            })
        }

        const {email, password} = req.body;

        const candidate =  await User.findOne({ email });

        if (candidate){
            return res.status(400).json({message: 'Такой пользователь уже существует'});
        }

        const  hashedPassword = await bcrypt.hash(password,12);
        const user = new User({email,password: hashedPassword});

        await user.save();

        res.status(201).json({message: 'Пользователь создан'});


    } catch (e) {
        res.status(500).json({message : 'Что-то пошло не так, попробуйте снова'});
    }
});

router.post('/login',
    [
        check('email','Введите корректный email').normalizeEmail().isEmail(),
        check('password','Минимальная длина пароля 6 сиволов')
            .exists().isLength({min: 4})//!!TODO
    ],
    async (req,res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при входе в систему'
                })
            }

            const {email, password} = req.body;

            const user =  await User.findOne({ email });

            if (!user){
                return res.status(400).json({message: 'Пользователь не найден'});
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (!isMatch){
                return res.status(400).json({message: 'Неверный пароль'});
            }

            const token = jwt.sign(
                {userId: user.id,person: user.personality, group:user.group },
                "fcsOMSU",
                {expiresIn: '24h'}
                );

            res.json({token, userId: user.id, person: user.personality, group: user.group});

        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так, попробуйте снова'});
        }
    }

);

module.exports = router;