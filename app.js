const express = require('express');
// const config = require('config');
const mongoose = require('mongoose');
const Stat = require('./models/Stat');
const path = require('path');
const Questions = require('./models/Questions');
const app = express();

app.use(express.json({ extended: true}));
app.use('/api/auth',require('./routes/auth.routes'));
app.use('/api/question',require('./routes/quest.routes'));

app.post('/detail/api/question/delstat', async (req, res) => {
    try {
        console.log(req.body.linkQ);
        const stat = await Questions.deleteOne({ _id: req.body.linkQ });
        const answer = await Stat.updateOne({ queLink: req.body.linkQ },{activity: false});
        res.json(answer);
    } catch (e) {
        res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' },e)
    }
});

if (process.env.NODE_ENV === 'production'){
    app.use('/',express.static(path.join(__dirname, 'client', 'build')));
    app.get('*',(req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    })
}


const PORT =  process.env.PORT || 5000
    // config.get('port');

async function start(){
    try{
        await mongoose.connect("mongodb+srv://admin:bibo808@uproject-9ubax.mongodb.net/test?retryWrites=true&w=majority",{
            useNewUrlParser : true,
            useUnifiedTopology : true,
            useCreateIndex : true
        });
        app.listen(PORT,() => console.log(`app has been started on ${PORT} port`));
    } catch (e) {
        console.log('Server Error', e.message);
        process.exit(1);
    }
}
start();

