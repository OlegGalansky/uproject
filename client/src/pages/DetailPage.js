import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useParams} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {Loader} from '../components/Loader'
import {FeedBackCard} from '../components/FeedBackCard'
import {StatCard} from "../components/StatCard";

export const DetailPage = () => {
    const {token,person} = useContext(AuthContext);
    const {request, loading} = useHttp();
    const [answer, setAnswer] = useState({});
    const answerId = useParams().id;

    const getFeedback = useCallback(async () => {
        try {
            const fetched = await request(`/api/question/${answerId}`, 'GET', null, {
                Authorization: `Bearer ${token}`
            });
            setAnswer(fetched);

        } catch (e) {}
    }, [token, answerId, request]);

    useEffect(() => {
        getFeedback()
    }, [getFeedback]);

    if (loading) {
        return <Loader/>
    }

    return (
        <>
            {!loading && answer.hasOwnProperty('answers') && <StatCard answer = {answer}/>}
            { !loading && person === 'student' && answer.hasOwnProperty('answer') && <FeedBackCard answer = {answer} /> }
        </>
    )
};