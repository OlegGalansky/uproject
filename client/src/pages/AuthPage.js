import React, {useContext, useEffect} from "react";
import {useState} from 'react';
import {useHttp} from '../hooks/http.hook'
import {useMassage} from '../hooks/message.hook'
import {AuthContext} from "../context/AuthContext";


export const AuthPage = () => {
    const auth = useContext(AuthContext);
    const {loading, error, request, clearError} = useHttp();
    const message = useMassage();

    const [form, setForm] = useState({
        email: '', password: ''
    });

    useEffect(() => {
        window.M.updateTextFields()
    },[]);

    useEffect(() => {
        message(error);
        clearError();
    },[error, message, clearError]);

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value});
    };

    const registerHandler = async () => {
        try {
            const data = await request('/api/auth/register','POST',{...form});
            message(data.message);
        } catch (e) {

        }
    };

    const loginHandler = async () => {
        try {
            const data = await request('/api/auth/login','POST',{...form});
            auth.login(data.token, data.userId, data.person,data.group);
        } catch (e) {

        }


    };

    return (
        <div className="row">
            <div className="col s6 offset-s3">
                <h1>  ФКН Контакт </h1>
                <div className="card red lighten-3">
                    <div className="card-content white-text">
                        <span className="card-title">Авторизация</span>
                        <div>

                            <div className="input-field">
                                <input
                                    placeholder="Введите email"
                                    id="email"
                                    type="text"
                                    name="email"
                                    className="input-color"
                                    value={form.email}
                                    onChange={changeHandler}
                                />
                                    <label htmlFor="first_name">Email</label>
                            </div>

                            <div className="input-field">
                                <input
                                    placeholder="Введите пароль"
                                    id="password"
                                    type="password"
                                    name="password"
                                    value={form.password}
                                    className="input-color"
                                    onChange={changeHandler}
                                />
                                <label htmlFor="first_name">Password</label>
                            </div>

                        </div>
                    </div>
                    <div className="card-action">
                        <button className="btn blue lighten-3"
                                style={{marginRight: 10}}
                                onClick={loginHandler}
                                disabled={loading}
                        >Войти
                            </button>
                        <button className="btn grey lighten-1"
                                onClick={registerHandler}
                                disabled={loading}
                        >Регистрация
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
};