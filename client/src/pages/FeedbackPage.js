import React, {useCallback} from "react";
import {useContext, useEffect} from "react";
import {useState} from 'react';
import {useHistory} from 'react-router-dom'
import {AuthContext} from "../context/AuthContext";
import {useHttp} from "../hooks/http.hook";
import {useMassage} from '../hooks/message.hook'
import {Loader} from "../components/Loader";

export const FeedbackPage = () => {
    const auth= useContext(AuthContext);
    const person = auth.person;
    const [form, setForm] = useState({activity: true, group: auth.group});
    const [coincidence, setCoincidence] = useState(true);
    const history = useHistory();
    const [questions, setQuestions] = useState({
        q1: "",
        q2: "",
        q3: "",
        q4: "",
        q5: "",
        q6: "",
        q7: "",
        q8: "",
        q9: "",
        q10: "",
        q11: "",
        q12: "",
        q13: "",
        q14: "",
        q15: ""});

    const {loading, error, request, clearError} = useHttp();
    const message = useMassage();

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value});
        console.log(form)

    };

    const getQuest = useCallback(async () => {
        try {
            const questionData = await request('api/question/getanswers','GET',null,{Authorization:`Bearer ${auth.token}`});
            console.log('Data',questionData);
            if (questionData.coincidence) {
               return  setCoincidence(true)
            }
            setCoincidence(false);
            setQuestions(questionData.questions);
            setForm({...form, name: questionData.name, id: questionData.id});

        } catch (e) {
            console.log('Data',e);
        }


    }, [auth,request]);

    const submitAnswer = async () => {
        try {
            const propertiesForm = Object.keys(form);
            console.log(propertiesForm);
            const questionsData = questions;
            if (propertiesForm.length !== 19) {
                message('Вы ввели ')
            } else if (propertiesForm.length === 19){
                const data = await request('api/question/setanswer','POST',{form, questionsData},{Authorization:`Bearer ${auth.token}`});
                console.log('Data',data);
                history.push(`/detail/${data.answer._id}`)

            }

        } catch (e) {

        }
    };

    const submitQuest = async () => {
        try {
            const propertiesForm = Object.keys(form);
            if (propertiesForm.length !== 18) {
                message('Вы ввели ')
            } else if (propertiesForm.length === 18){
                const data = await request('api/question/setquest','POST',{form},{Authorization:`Bearer ${auth.token}`});
                console.log('Data',data);
                history.push(`/detail/${data.result._id}`)


            }

        } catch (e) {

        }
    };

    useEffect(() => {
        window.M.updateTextFields()
    },[]);


    useEffect(() => {
        if (person === 'student'){
            getQuest()
        }
    },[getQuest]);

    if (loading){
        return <Loader />
    }

    if (coincidence && person === 'student'){
        return <p className="center">Вопросов пока нет</p>
    }



    return(
        <div>
            {person === 'student' ? (
                <div>
                    <p className="flow-text">{form.name}</p>
                    <p className="text">{questions.q1}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q1"
                                onChange={changeHandler}>
                            <option value="a1" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text"
                        defaultValue={questions.q2} >{questions.q2}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q2"
                                onChange={changeHandler}>
                            <option value="a2" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q3}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                onChange={changeHandler}
                                name="q3">
                            <option value="a3" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q4}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                onChange={changeHandler}
                                name="q4">
                            <option value="a4" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q5}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                onChange={changeHandler}
                                name="q5">
                            <option value="a5" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q6}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q6"
                                onChange={changeHandler}>
                            <option value="a6" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q7}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q7"
                                onChange={changeHandler}>
                            <option value="a7" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q8}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q8"
                                onChange={changeHandler}>
                            <option value="a8" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q9}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q9"
                                onChange={changeHandler}>
                            <option value="a9" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q10}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q10"
                                onChange={changeHandler}>
                            <option value="a10" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q11}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q11"
                                onChange={changeHandler}>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q12}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q12"
                                onChange={changeHandler}>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q13}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q13"
                                onChange={changeHandler}>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q14}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q14"
                                onChange={changeHandler}>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text">{questions.q15}</p>
                    <div className="input-field col s12">
                        <select className="browser-default"
                                name="q15"
                                onChange={changeHandler}>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>

                        </select>
                        <label></label>
                    </div>

                    <p className="text
                    ">Проверьте, что вы заполнили все поля</p>

                    <div style={{paddingBottom: '2rem'}}>
                        <button className="waves-effect waves-light btn"
                                type="button"
                                onClick={submitAnswer}
                                name="action">Submit
                        </button>

                    </div>




                </div>
            ) : (
                <div>
                    <p className="flow-text">Создайте опрос : </p>
                    <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    <p className="flow-text">Группа</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите группу"
                            id="group"
                            type="text"
                            name="group"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Название</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите название"
                            id="name"
                            type="text"
                            name="name"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}

                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №1</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q1"
                            type="text"
                            name="q1"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}

                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №2</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q2"
                            type="text"
                            name="q2"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №3</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q3"
                            type="text"
                            name="q3"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №4</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q4"
                            type="text"
                            name="q4"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №5</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q5"
                            type="text"
                            name="q5"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №6</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q6"
                            type="text"
                            name="q6"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №7</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q7"
                            type="text"
                            name="q7"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №8</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q8"
                            type="text"
                            name="q8"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №9</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q9"
                            type="text"
                            name="q9"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №10</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q10"
                            type="text"
                            name="q10"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №11</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q11"
                            type="text"
                            name="q11"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №12</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q12"
                            type="text"
                            name="q12"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №13</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q13"
                            type="text"
                            name="q13"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №14</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q14"
                            type="text"
                            name="q14"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="flow-text">Вопрос №15</p>
                    <div className="input-field">
                        <input
                            placeholder="Введите вопрос"
                            id="q15"
                            type="text"
                            name="q15"
                            // value={form.password}
                            className="input-color"
                            onChange={changeHandler}
                        />
                        <label htmlFor="first_name">Этот вопрос обязателен*</label>
                    </div>

                    <p className="text
                    ">Проверьте, что вы заполнили все поля</p>



                    <div style={{paddingBottom: '2rem'}}>
                        <button className="waves-effect waves-light btn"
                            type="button"
                            onClick={submitQuest}
                            name="action">Submit
                        </button>

                    </div>

                </div>

            )}
        </div>
    )
};