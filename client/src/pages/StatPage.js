import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContext'
import {Loader} from '../components/Loader'
import {FeedBackCardList} from '../components/FeedBackCardList'
import {StatCardList} from "../components/StatCardList";

export const StatPage = () => {
    const [answer, setAnswer] = useState([]);
    const {loading, request} = useHttp();
    const {token,person} = useContext(AuthContext);

    const fetchStat = useCallback(async () => {
        try {
            const fetched = await request('/api/question', 'GET', null, {
                Authorization: `Bearer ${token}`
            });
            setAnswer(fetched)
        } catch (e) {}
    }, [token, request]);

    useEffect(() => {
        fetchStat();
    }, [fetchStat]);

    if (loading) {
        return <Loader/>
    }

    return (
        <>
            {!loading && person === 'admin' &&  <StatCardList answer = {answer}/>}
            {!loading && person === 'student' && <FeedBackCardList answer={answer} />}
        </>
    )
};