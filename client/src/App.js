import React from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import {useRoutes} from './routes'
import 'materialize-css'
import {useAuth} from "./hooks/auth.hook";
import {AuthContext} from "./context/AuthContext";
import {StudentNavbar} from "./components/StudentNavbar";
import {Loader} from "./components/Loader";


function App() {
    const {token, login, logout, userId, person, group, ready} = useAuth();
    const isAuthenticated = !!token;
    const routes = useRoutes(isAuthenticated);

    if (!ready){
        return <Loader />
    }

    return (
      <AuthContext.Provider value= {{
        token, userId, login, logout, isAuthenticated, person, group, ready
      }}>
        <Router>
            { isAuthenticated && <StudentNavbar/> }
          <div className="container">
              {routes}
          </div>
        </Router>
      </AuthContext.Provider>
    );
}

export default App


