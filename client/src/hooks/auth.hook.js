import {useCallback, useState, useEffect} from "react";
const storageName = 'userData';

export const useAuth = () => {
    const [token, setToken] = useState(null);
    const [userId, setUserId] = useState(null);
    const [person, setPerson] = useState(null);
    const [ready, setReady] = useState(false);
    const [group, setGroup] = useState(null);

    const login = useCallback((jwtToken,id,pers,group) => {
        setToken(jwtToken);
        setUserId(id);
        setPerson(pers);
        setGroup(group);
        localStorage.setItem(storageName, JSON.stringify({userId: id, token: jwtToken, person: pers, group: group}));//TODO скорее всего не нужно хранить все кроме пароля
    },[]);

    const logout = useCallback(() => {
        setUserId(null);
        setToken(null);
        setPerson(null);
        setGroup(null);
        localStorage.removeItem(storageName)
    }, []);

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName));

        if (data && data.token){
            login(data.token,data.userId,data.person,data.group)
        }

        setReady(true);

    },[login]);

    return{login, logout, userId, token, person, group, ready}
};