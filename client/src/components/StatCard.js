import React, {useCallback, useContext} from 'react'
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts'
import Collapsible from 'react-collapsible';
import {useHttp} from "../hooks/http.hook";
import {AuthContext} from "../context/AuthContext";


export const StatCard = ({ answer }) => {
    const {request} = useHttp();
    const auth= useContext(AuthContext);
    const questions = answer.questions;
    const queArr = Object.keys(questions);
    const linkQ = answer.queLink;
    let chartMark;


    const data = [];
    for (const key in answer.answers){
        chartMark = answer.answers[key] / answer.count;
        if (isNaN(chartMark) || chartMark === Infinity){
            chartMark = 0;
        }

        data.push({name :[key], оценка: chartMark});
    }
    console.log(data);

    const delStat = useCallback(async () => {
        try {
            console.log(answer);
            const questionData = await request('api/question/delstat','POST',{linkQ},{Authorization:`Bearer ${auth.token}`});
            console.log('Data',questionData);
        } catch (e) {
            console.log('Data',e);
        }


    }, [auth,request,answer]);

    // const answers = Object.values(stat);
    // const quetions = Object.values(stat);>\

    // if (!answers.length) {
        // return <p className="center">Ссылок пока нет</p>
    // }

    return (
        <div>
            <p className="text">{answer.name}</p>
            <p className="text">{answer.group}</p>
            <Collapsible style="flow-text" trigger="•Список вопросов">
                {queArr.map((question,index) => {
                   return <p className="text">Вопрос №{index+1}: {questions[question]}</p>
                })}
            </Collapsible>



        <BarChart width={800} height={400} data={data}
                  margin={{top: 50, right: 10, left: 100, bottom: 5}}>
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="name"/>
            <YAxis/>
            <Tooltip/>
            <Legend />
            <Bar dataKey="оценка" fill="#8884d8"  />
        </BarChart>

            <p className="text">Количество ответов : {answer.count}</p>
            <div style={{paddingBottom: '2rem'}}>
                <button className="red waves-effect waves-light  btn"
                        type="button"
                        onClick={delStat}
                        name="action">Завершить
                </button>

            </div>

        </div>
    );
};