import React from 'react'
import {Link} from 'react-router-dom'

export const StatCardList = ({ answer }) => {
    if (!answer.length) {
        return <p className="center">Статистики пока нет</p>
    }
    console.log(answer);

    return (
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>Группа</th>
                <th>Название</th>
                <th>Активность</th>
                <th>Количество</th>
                <th>Статистика</th>
            </tr>
            </thead>

            <tbody>
            { answer.map((answer, index) => {
                return (
                    <tr key={answer._id}>
                        <td>{index + 1}</td>
                        <td>{answer.group}</td>
                        <td>{answer.name}</td>
                        <td>{answer.activity ? ('Активен'):('Завершен')}</td>
                        <td>{answer.count}</td>
                        <td>
                            <Link to={`/detail/${answer._id}`}>Открыть</Link>
                        </td>
                    </tr>
                )
            }) }
            </tbody>
        </table>
    )
};