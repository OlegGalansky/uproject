import React from 'react'
import {Link} from 'react-router-dom'

export const FeedBackCardList = ({ answer }) => {
    if (!answer.length) {
        return <p className="center">Ссылок пока нет</p>
    }
    console.log(answer);

    return (
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>Название</th>
                <th>Ответ</th>
            </tr>
            </thead>

            <tbody>
            { answer.map((answer, index) => {
                return (
                    <tr key={answer._id}>
                        <td>{index + 1}</td>
                        <td>{answer.name}</td>
                        {/*<td>{answer.answer['q'+ index + a]}</td>*/}
                        <td>
                            <Link to={`/detail/${answer._id}`}>Открыть</Link>
                        </td>
                    </tr>
                )
            }) }
            </tbody>
        </table>
    )
};