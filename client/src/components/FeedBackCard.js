import React from 'react'
import {Link} from 'react-router-dom'

export const FeedBackCard = ({ answer }) => {

    console.log(answer)
    const answers = Object.values(answer.answer);
    const quetions = Object.values(answer.question);

    if (!answers.length) {
        return <p className="center">Пока нет заполненых вопросов</p>
    }


    return (
        <table className="col s7 push-s5">
            <thead>
            <tr>
                <th>№</th>
                <th>Вопрос</th>
                <th>Оценка</th>
            </tr>
            </thead>

            <tbody>
            {
                answers.map((answer, index) => {
                return (
                    <tr key={answer._id}>
                        <td>{index + 1}</td>
                        <td>{quetions[index]}</td>
                        <td>{answers[index]}</td>
                        {/*<td>{answer.answer['q'+ index + a]}</td>*/}
                    </tr>
                )
            })
            }
            </tbody>
        </table>
    )
};