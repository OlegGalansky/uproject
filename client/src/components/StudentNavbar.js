import React, {useContext, useEffect,} from "react";
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from "../context/AuthContext";
import M from  'materialize-css/dist/js/materialize.min.js';

export const StudentNavbar = () => {

    const auth = useContext(AuthContext);
    const history = useHistory();
    const logoutHandler = event => {
        event.preventDefault();
        auth.logout();
        history.push('/');
    };
    useEffect(() => {
        let sidenav = document.querySelector('#slide-out');
        M.Sidenav.init(sidenav, {});
    });



        return (
            <div>
                <nav>
                    <div className="nav-wrapper">
                        <a href="#" data-target="slide-out" className="sidenav-trigger show-on-large"><i
                            className="material-icons">menu</i></a>
                        <a href="/" className="brand-logo" >ФКН Контакт</a>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            <li><NavLink to="/feedback">Отзывы</NavLink></li>
                            <li><NavLink to="/stat">Статистика</NavLink></li>
                            <li><a href="/" onClick={logoutHandler}>Выйти</a></li>
                        </ul>
                    </div>
                </nav>

                <ul id="slide-out" className="sidenav">
                    <li><NavLink to="/feedback">Отзывы</NavLink></li>
                    <li><NavLink to="/stat">Статистика</NavLink></li>
                    <li><a href="/" onClick={logoutHandler}>Выйти</a></li>
                </ul>

            </div>
        )

};