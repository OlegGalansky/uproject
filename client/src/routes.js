import React from 'react'
import {Switch, Route, Redirect, useHistory} from 'react-router-dom'
import {StatPage} from './pages/StatPage'
import {FeedbackPage} from './pages/FeedbackPage'
import {DetailPage} from './pages/DetailPage'
import {AuthPage} from './pages/AuthPage'

export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Switch>
                <Route path="/stat" exact>
                    <StatPage />
                </Route>
                <Route path="/feedback" exact>
                    <FeedbackPage />
                </Route>
                <Route path="/detail/:id">
                    <DetailPage />
                </Route>
                <Redirect to = '/stat' />
            </Switch>
        )
    }

    return (
        <Switch>
            <Route path="/" exact>
                <AuthPage />
            </Route>
            <Redirect to="/" />
        </Switch>
    )
};