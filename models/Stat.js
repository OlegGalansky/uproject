const {Schema, model, Types} = require('mongoose');

schema = new Schema({
    group: {type: String, required: true},
    answers:  {
        type: Map,
        of: Number
    },
    questions:  {
        type: Map,
        of: String
    },
    count: {type: Number, required: true},
    activity: {type: Boolean, required: true},
    owner: {type: Types.ObjectId, ref: 'User'},
    queLink: {type: Types.ObjectId, ref: 'Questions'},
    name: {type: String, required: true}

});
module.exports = model('Stat', schema);