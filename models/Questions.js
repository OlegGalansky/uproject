const {Schema, model, Types} = require('mongoose');

schema = new Schema({
    group: {type: String, required: true},
    questions:  {
        type: Map,
        of: String
    },

    activity: {type: Boolean, required: true},
    name: {type: String, required: true},
    stat: [{ type: Types.ObjectId, ref: 'Stat'}],
    answer: [{ type: Types.ObjectId, ref: 'Answer'}]

});

module.exports = model('Questions', schema);