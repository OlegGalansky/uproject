const {Schema, model, Types} = require('mongoose');

schema = new Schema({
    group: {type: String, required: true},
    answer:  {
        type: Map,
        of: String
    },
    question:  {
        type: Map,
        of: String
    },
    activity: {type: Boolean, required: true},
    owner: {type: Types.ObjectId, ref: 'User'},
    queLink: {type: Types.ObjectId, ref: 'Questions'},
    date: {type: Date, default: Date.now()},
    name: {type: String, required: true}


});

module.exports = model('Answer',schema);