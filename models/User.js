const {Schema, model, Types} = require('mongoose');

schema = new Schema({
    email : {type: String, required: true, unique: true},
    password: {type: String, required: true},
    personality: {type: String, required: true},
    group: {type: String, required: true},
    answers: [{ type: Types.ObjectId, ref: 'Answer'}],
    stat: [{ type: Types.ObjectId, ref: 'Stat'}]
});

module.exports = model('User', schema);

// mongoimport --host uproject-shard-00-01-9ubax.mongodb.net:27017 --db test --collection users --file users1.json --jsonArray --authenticationDatabase admin --ssl --username admin --password bibo808