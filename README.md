**FCS FEEDBACK**

*Мой проект созданный в рамках учебной практики второго симестра.*

* Используемый стек технологий : MONGO,EXPRESS,REACT,NODE.JS

* Сайт : [http://fcsfeedback.herokuapp.com/](url)

* Ссылка на призентацию проекта :  [https://drive.google.com/file/d/1X0zs8aLs8ByOx4yI2aXiovMgXl9fXYYA/view?usp=sharing](url)

Тестовые аккаунты : 

* admin: *test@gmail.com test*
* user: *test3@gmail.com test*
