
module.exports = (req, res, next) => {

    const {form} = req.body;
    const propertiesForm = Object.keys(form);

    let reqData = {questions: {}};
    let s = 'q0';
    let i = 0;

    for (let key of propertiesForm){
        let str = s[0] + (+s[1] + ++i);
        if (key === 'activity') {
            reqData[key] = form[key];
            i--;
        } else if (key === 'group') {
            reqData[key] = form[key];
            i--;

        } else if (key === 'name') {
            reqData[key] = form[key];
            i--;
        } else if (key === 'id') {
            reqData[key] = form[key];
            i--;
        }else reqData.questions[str] = form[str];

    }
    req.questions = reqData.questions;
    req.name = reqData.name;
    req.group = reqData.group;
    req.activity = reqData.activity;
    req.id = reqData.id;

    next ();

};